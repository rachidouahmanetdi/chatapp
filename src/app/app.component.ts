import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import * as firebase from 'firebase';

export const snapshotToArray = (snapshot) => {
  const returnArr = [];
  snapshot.forEach((childSnapshot) => {
    const item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};

const supportUsers = [
  {
    nickname: 'SALAH ST',
    username: 'SALAH@TEST.com',
  },
];

const Contact = [
  {
    username: 'peaqock@sopriamoccasions.ma',
  },
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  isVisible = false;
  roomRef 
  contactRef 
  checkoutForm: any;


  constructor(
    private formBuilder: FormBuilder,
  ) {
    const firebaseConfig = {
        projectId: 'chatmodule-fce56',
        apiKey: 'AIzaSyBjIMmV7Ph0EPX4KN7zsUDXv2qo9Jem74Y',
        databaseURL: 'https://chatmodule-fce56-default-rtdb.firebaseio.com/',
      };
      firebase.initializeApp(firebaseConfig);

      this.roomRef = firebase.database().ref('rooms/');
      this.contactRef = firebase.database().ref('contacts/');
    this.checkoutForm = this.formBuilder.group({
      name: '',
      address: ''
    });
  }

  title = 'chatApp';


  showModal(): void {
    this.isVisible = true;
  }

  closeModal(): void {
    this.isVisible = false;
  }
  onSubmit(customerData) {
    // Process checkout data here
    this.ConnectToChat(customerData.name,customerData.address)
    this.checkoutForm.reset();

    console.warn('Your order has been submitted', customerData);
  }
  ConnectToChat(username,mail): void {
    this.contactRef
      .orderByChild('username')
      .equalTo(supportUsers[0].username)
      .once('value', (snapshot) => {
        console.log(snapshot);
        const newUser = firebase.database().ref('contacts/').push();
        newUser.set(supportUsers[0]);
        console.log('Button ok clicked!');


      });
  }

  SelectContact(): void {
    const roomname = `${supportUsers[0].username}_${Contact[0].username}`;
    this.roomRef
      .orderByChild('roomname')
      .equalTo(`${supportUsers[0].username}_${Contact[0].username}`)
      .once('value', (snapshot) => {
        const newRoom = this.roomRef.push();
        newRoom.set({roomname});
      });
    this.enterChatRoom(roomname);
  }

  enterChatRoom(roomname): void {
    const chat = {
      roomname: '',
      nickname: '',
      message: '',
      date: '',
      type: '',
      username: ''
    };
    chat.roomname = roomname;
    chat.nickname = supportUsers[0].nickname;
    chat.username = supportUsers[0].username;
    chat.date = '';
    chat.message = `${supportUsers[0].nickname} entrer`;
    chat.type = 'join';
    const newMessage = firebase.database().ref('chats/').push();
    newMessage.set(chat);
    firebase
      .database()
      .ref('roomusers/')
      .orderByChild('roomname')
      .equalTo(roomname)
      .on('value', (resp) => {
        let roomuser;
        roomuser = snapshotToArray(resp);
        const user = roomuser.find((x) => x.username === supportUsers[0].username);
        if ((user)) {
          const userRef = firebase.database().ref('roomusers/' + user.key);
          userRef.update({status: 'online'});
        } else {
          const newroomuser = {
            roomname: '',
            username: '',
            nickname: '',
            status: '',
          };
          newroomuser.roomname = roomname;
          newroomuser.nickname = supportUsers[0].nickname;
          newroomuser.username = supportUsers[0].username;
          newroomuser.status = 'online';
          const newRoomUser = firebase.database().ref('roomusers/').push();
          newRoomUser.set(newroomuser);
        }
      });
  }
}