import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import firebase from 'firebase'
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// const firebaseConfig = {
//   projectId: 'chatmodule-fce56',
//   apiKey: 'AIzaSyBjIMmV7Ph0EPX4KN7zsUDXv2qo9Jem74Y',
//   databaseURL: 'https://chatmodule-fce56-default-rtdb.firebaseio.com/',
// };
// firebase.initializeApp(firebaseConfig);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
